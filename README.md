# Platform
In this Directory the repositories needed for the platform are collected.

The [Admin Guide](https://gitlab.com/berlintxl/futr-hub/platform/data-platform/-/blob/master/00_documents/INSTALL.md) will help you install the platform while the [User Guide](https://gitlab.com/berlintxl/futr-hub/platform/data-platform/-/blob/master/00_documents/GETTING_STARTED.md) will help on utilizing it.  
If you want to be part of the team, read about how to participate in the [Contribution Guide](https://gitlab.com/berlintxl/futr-hub/platform/data-platform/-/blob/master/00_documents/CONTRIBUTING.md).

## Architectural Overview
### V1 on Docker
Is being migrated to V2 on Kubernetes

[link to repository](https://gitlab.com/berlintxl/futr-hub/platform/data-platform)

![Architectural Overview](./diagrams/futr_hub_architecture.png)

### V2 on Kubernetes
Is developed actively and currently in the process of migration

[link to repository](https://gitlab.com/berlintxl/futr-hub/platform/data-platform-k8s)

![Architectural Overview](./diagrams/futr_hub_architecture_k8s.png)


## License
This work is licensed under [EU PL 1.2](LICENSE) by the State of Berlin, Germany, represented by [Tegel Projekt GmbH](https://www.tegelprojekt.de/). Please see the [list of authors](https://gitlab.com/berlintxl/futr-hub/getting-started/-/blob/master/AUTHORS-ATTRIBUTION.md) and [list of contributors](https://gitlab.com/berlintxl/futr-hub/getting-started/-/blob/master/LIST-OF-CONTRIBUTORS.md).

All contributions to this repository from January 1st 2020 on are considered to be licensed under the EU PL 1.2 or any later version.
This project doesn't require a CLA (Contributor License Agreement). The copyright belongs to all the individual contributors. For further information, please see the [guidelines for contributing](https://gitlab.com/berlintxl/futr-hub/getting-started/-/blob/master/CONTRIBUTING.md).
